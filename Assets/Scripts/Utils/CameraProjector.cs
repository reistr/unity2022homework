﻿using UnityEngine;

namespace Scripts.Utils
{
    public sealed class CameraProjector
    {
        private Camera camera; 

        public CameraProjector(Camera camera)
        {
            this.camera = camera;
        }
        
        public bool TryGetProjectedPoint(Vector3 position, out Vector3 projectedPoint)
        {
            Ray ray = this.camera.ScreenPointToRay(position);
            
            RaycastHit hit;
            if (Physics.Raycast(ray.origin, ray.direction, out hit, Mathf.Infinity, LayerMask.GetMask("BasePlane")))
            {
                projectedPoint = hit.point;
                return true;
            }
            
            projectedPoint = Vector3.positiveInfinity;
            return false;
        }
    }
}