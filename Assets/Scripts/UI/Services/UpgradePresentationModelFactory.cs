﻿using GameElements;
using Scripts.Models;
using Scripts.Services;
using Scripts.UI.Popups;
using UnityEngine;

namespace Scripts.UI.Services
{
    public sealed class UpgradePresentationModelFactory : MonoBehaviour, IGameInitElement
    {
        private UpgradeService upgradeService;

        public UpgradePresentationModel CreatePresenter(Upgrade[] upgrades)
        {
            return new UpgradePresentationModel(this.upgradeService, upgrades);
        }
        
        void IGameInitElement.InitGame(IGameContext context)
        {
            this.upgradeService = context.GetService<UpgradeService>();
        }
    }
}