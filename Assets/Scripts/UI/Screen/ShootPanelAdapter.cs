﻿using System;
using GameElements;
using Scripts.Services;
using UnityEngine;

namespace Scripts.UI.Screen
{
    public class ShootPanelAdapter : MonoBehaviour,
        IGameInitElement,
        IGameStartElement,
        IGameFinishElement
    {
        [SerializeField] private ShootPanel shootPanel;

        private BulletShooter bulletShooter;

        void IGameInitElement.InitGame(IGameContext context)
        {
            this.shootPanel.SetupData();
            this.bulletShooter = context.GetService<BulletShooter>();
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.bulletShooter.OnShootStart += BulletShooterOnShootStart;
            this.bulletShooter.OnShootEnd += BulletShooterOnShootEnd;
        }

        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.bulletShooter.OnShootStart -= BulletShooterOnShootStart;
            this.bulletShooter.OnShootEnd -= BulletShooterOnShootEnd;
        }
        
        private void BulletShooterOnShootStart()
        {
            this.bulletShooter.OnShootProgress += BulletShooterOnShootProgress;
            this.shootPanel.UpdateData(shootInProgress: true);
        }

        private void BulletShooterOnShootProgress(float progress)
        {
            var newValue = Convert.ToByte(progress * 100);
            this.shootPanel.UpdateData(true, newValue);
        }

        private void BulletShooterOnShootEnd()
        {
            this.bulletShooter.OnShootProgress -= BulletShooterOnShootProgress;
            this.shootPanel.UpdateData(shootInProgress: false);
        }
    }
}