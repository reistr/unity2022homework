﻿using TMPro;
using UnityEngine;

namespace Scripts.UI.Screen
{
    public class ShootPanel : MonoBehaviour
    {
        [SerializeField] 
        private GameObject checkmark;

        [SerializeField] 
        private TextMeshProUGUI progress;
        
        public void SetupData()
        {
            this.progress.enabled = false;
            this.checkmark.SetActive(true);
        }

        public void UpdateData(bool shootInProgress, int progress = 0)
        {
            this.checkmark.SetActive(!shootInProgress);
            if (shootInProgress)
            {
                this.progress.enabled = true;
                this.progress.text = progress + "%";
            }
            else
            {
                this.progress.enabled = false;
            }
        }
    }
}