﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Models;
using Scripts.Services;
using TMPro;

namespace Scripts.UI.Popups
{
    public sealed class UpgradePresentationModel : UpgradePopup.IPresentationModel
    {
        public event Action<bool> OnUpgradeAvailableChanged;

        public bool IsUpgradeAvailable => this.upgradeService.CanUpgrade(this.selectedUpgrade.type);

        private UpgradeService upgradeService;

        private Upgrade selectedUpgrade;

        private Upgrade[] upgrades;
        
        public UpgradePresentationModel(UpgradeService upgradeService, Upgrade[] upgrades)
        {
            this.upgradeService = upgradeService;
            this.upgrades = upgrades;
            this.selectedUpgrade = this.upgrades.First();
        }

        public List<TMP_Dropdown.OptionData> GetOptions()
        {
            var result = new List<TMP_Dropdown.OptionData>();
            foreach (var upgrade in this.upgrades)
            {
                result.Add(new TMP_Dropdown.OptionData(upgrade.label));
            }

            return result;
        }

        public void SetSelectedOption(int index)
        {
            if (index >= 0 && index < this.upgrades.Length)
            {
                this.selectedUpgrade = this.upgrades[index];
                var canUpgrade = this.upgradeService.CanUpgrade(this.selectedUpgrade.type);
                this.OnUpgradeAvailableChanged?.Invoke(canUpgrade);
            }
            else
            {
                throw new ArgumentException("Upgrade type index is out of bounds");
            }
        }
        
        public void OnUpgradeClicked()
        {
            this.upgradeService.UpgradeHero(this.selectedUpgrade.type);
        }
    }
}