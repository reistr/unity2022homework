﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Scripts.UI.Popups
{
    public class UpgradePopup : Popup
    {
        [SerializeField]
        private Button upgradeButton;

        [SerializeField]
        private TMP_Dropdown dropdown;
        
        private IPresentationModel presenter;

        protected override void OnShow(object args)
        {
            if (args is not IPresentationModel presenter)
            {
                throw new ArgumentException("Wrong presentation model");
            }

            this.presenter = presenter;
            this.upgradeButton.onClick.AddListener(this.UpgradeButtonClick);
            this.upgradeButton.interactable = this.presenter.IsUpgradeAvailable;
            this.presenter.OnUpgradeAvailableChanged += PresenterOnUpgradeAvailableChanged;

            this.dropdown.options = this.presenter.GetOptions();
            this.dropdown.onValueChanged.AddListener(this.SelectedUpgradeChanged);
        }

        private void PresenterOnUpgradeAvailableChanged(bool canUpgrade)
        {
            this.upgradeButton.interactable = canUpgrade;
        }

        protected override void OnHide()
        {
            this.upgradeButton.onClick.RemoveListener(this.UpgradeButtonClick);
            this.presenter.OnUpgradeAvailableChanged -= PresenterOnUpgradeAvailableChanged;
            this.dropdown.onValueChanged.RemoveListener(this.SelectedUpgradeChanged);
        }

        private void UpgradeButtonClick()
        {
            this.presenter.OnUpgradeClicked();
        }

        private void SelectedUpgradeChanged(int index)
        {
            this.presenter.SetSelectedOption(index);
        }
        
        public interface IPresentationModel
        {
            public bool IsUpgradeAvailable { get; }
            
            event Action<bool> OnUpgradeAvailableChanged;
                
            void OnUpgradeClicked();

            List<TMP_Dropdown.OptionData> GetOptions();

            void SetSelectedOption(int index);
        }
    }
}