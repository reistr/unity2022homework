﻿using System;
using System.Collections;
using UnityEngine;

namespace Scripts
{
    public sealed class StartCounter : MonoBehaviour
    {
        public event Action<int> OnTick;

        public event Action OnEnd;

        [SerializeField]
        private int duration = 3;
        
        private Coroutine countdownCoroutine;

        public void StartCountdown()
        {
            if (this.countdownCoroutine == null)
            {
                this.countdownCoroutine = this.StartCoroutine(this.CountdownRoutine());
            }
        }

        private IEnumerator CountdownRoutine()
        {
            var period = new WaitForSeconds(1);
            var counter = this.duration;
            
            while (counter > 0)
            {
                this.OnTick?.Invoke(counter);
                yield return period;
                counter--;
            }

            this.OnTick?.Invoke(counter);
            this.countdownCoroutine = null;
            this.OnEnd?.Invoke();
        }
    }
}