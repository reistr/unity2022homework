﻿using GameElements.Unity;
using UnityEngine;

namespace Scripts
{
    public sealed class GameManager : MonoBehaviour
    {
        [SerializeField] private MonoGameContext GameContext;
        [SerializeField] private StartCounter counter;
        
        private void Start()
        {
            this.InitializeGame();
            this.counter.OnTick += LogCount;
            this.counter.OnEnd += StartGame;
            this.counter.StartCountdown();
        }

        private void InitializeGame()
        {
            this.GameContext.LoadGame();
            this.GameContext.InitGame();
            this.GameContext.ReadyGame();
        }

        private void LogCount(int count)
        {
            Debug.Log(count == 0 ? "Start!" : count);
        }
        
        private void StartGame()
        {
            this.GameContext.StartGame();
            this.Unsubscribe();
        }

        private void Unsubscribe()
        {
            this.counter.OnTick -= LogCount;
            this.counter.OnEnd -= StartGame;
        }
    }
}