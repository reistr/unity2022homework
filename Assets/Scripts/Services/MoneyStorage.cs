﻿using UnityEngine;

namespace Scripts.Services
{
    public sealed class MoneyStorage : MonoBehaviour
    {
        private int money;

        public int GetMoney()
        {
            return this.money;
        }

        public void SetMoney(int money)
        {
            this.money = money;
        }
    }
}