﻿using UnityEngine;

namespace Scripts.Services
{
    public sealed class GroundService : MonoBehaviour
    {
        [SerializeField]
        private MeshFilter planeMesh;

        [SerializeField] private Transform planeTransform;

        public bool IsOnGround(Vector3 position, Bounds bounds)
        {
            var planeRelated = planeTransform.InverseTransformPoint(position);
            var planeRelatedBounds = new Bounds {center = planeRelated, extents = bounds.extents};
            return planeMesh.mesh.bounds.Intersects(planeRelatedBounds);
        }
    }
}