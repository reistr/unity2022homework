using Scripts.Utils;
using UnityEngine;

namespace Scripts.Services
{
    public sealed class CameraService : MonoBehaviour
    {
        [SerializeField]
        private Transform cameraTransform;

        [SerializeField]
        private new Camera camera;

        [SerializeField]
        private Vector3 offset;
        
        private CameraProjector cameraProjector;

        private void Awake()
        {
            this.cameraProjector = new CameraProjector(this.Camera);
        }

        public Camera Camera
        {
            get { return this.camera; }
        }

        public Transform CameraTransform
        {
            get { return this.cameraTransform; }
        }

        public Vector3 Offset
        {
            get { return this.offset; }
        }

        public CameraProjector CameraProjector
        {
            get { return this.cameraProjector; }
        }
    }
}