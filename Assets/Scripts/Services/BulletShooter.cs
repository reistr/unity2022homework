﻿using System;
using System.Collections;
using Scripts.Primitives.Beahviours;
using UnityEngine;

namespace Scripts.Services
{
    public sealed class BulletShooter : MonoBehaviour
    {
        public event Action OnShootStart;
        public event Action OnShootEnd;
        public event Action<float> OnShootProgress;
        
        [SerializeField]
        private GameObject bulletPrototype;

        private float bulletMaxDistance;
        
        private float bulletSpeed;
        
        private Vector3 targetDirection;

        private Vector3 originalPosition;

        private Coroutine coroutine = null;

        public bool InProgress => this.coroutine != null;
        
        public void Shoot(Vector3 shooterPosition, Vector3 targetPosition, float bulletSpeed, float maxDistance)
        {
            if (this.InProgress)
            {
                Debug.LogError("Shoot in progress");
                return;
            }

            if (shooterPosition == targetPosition)
            {
                Debug.LogError("Wrong arguments");
                return;
            }
            
            this.LaunchBullet(shooterPosition, targetPosition, bulletSpeed, maxDistance);
        }

        private void LaunchBullet(Vector3 shooterPosition, Vector3 targetPosition, float bulletSpeed, float maxDistance)
        {
            this.originalPosition = shooterPosition;
            this.targetDirection = (targetPosition - shooterPosition).normalized;
            this.bulletSpeed = bulletSpeed;
            this.bulletMaxDistance = maxDistance;
            
            this.bulletPrototype.transform.position = this.originalPosition;
            this.bulletPrototype.SetActive(true);
            
            this.coroutine = this.StartCoroutine(this.MoveBullet());
            this.OnShootStart?.Invoke();
        }
        
        private IEnumerator MoveBullet()
        {
            for (;;)
            {
                this.bulletPrototype.transform.position += this.targetDirection * (this.bulletSpeed * Time.deltaTime);
                var distance = Vector3.Distance(this.originalPosition, this.bulletPrototype.transform.position);

                if (distance >= bulletMaxDistance)
                {
                    this.bulletPrototype.transform.position = Vector3.zero;
                    this.bulletPrototype.SetActive(false);

                    this.StopCoroutine(this.coroutine);
                    this.coroutine = null;
                    this.OnShootEnd?.Invoke();
                }
                else
                {
                    this.OnShootProgress?.Invoke(distance/bulletMaxDistance);
                }

                yield return null;
            }
        }
    }
}