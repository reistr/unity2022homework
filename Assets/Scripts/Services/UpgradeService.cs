﻿using System;
using Entities;
using GameElements;
using Scripts.Models;
using UnityEngine;

namespace Scripts.Services
{
    public sealed class UpgradeService : MonoBehaviour, IGameInitElement
    {
        private IEntity hero;
        
        private UpgradesStorage storage;

        public bool CanUpgrade(UpgradeType upgradeType)
        {
            switch (upgradeType)
            {
                case UpgradeType.Speed:
                    return true;
                case UpgradeType.MaxHitPoints:
                    return true;
                case UpgradeType.BulletSpeed:
                    return true;
                case UpgradeType.BulletDistance:
                    return false;
                default:
                    return false;
            }
        }
        
        public void UpgradeHero(UpgradeType upgradeType)
        {
            if (this.CanUpgrade(upgradeType))
            {
                var upgrade = this.storage.GetUpgrade(upgradeType);
                if (upgrade == null)
                {
                    throw new Exception($"Upgrade {upgradeType} was not found");
                }
                this.storage.SetUpgrade(upgradeType, upgrade.currentLevel + 1);
            }
            
        }
        
        void IGameInitElement.InitGame(IGameContext context)
        {
            this.hero = context.GetService<HeroService>()
                .GetHero();
            this.storage = context.GetService<UpgradesStorage>();
        }
    }
}