﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Scripts.Models;
using UnityEngine;

namespace Scripts.Services
{
    // Stub
    public class UpgradesStorage : MonoBehaviour
    {
        private Upgrade[] heroUpgrades = new[]
        {
            new Upgrade(UpgradeType.Speed, "Speed", 0),
            new Upgrade(UpgradeType.MaxHitPoints, "HP", 0),
            new Upgrade(UpgradeType.BulletSpeed, "Upgrade bullet speed", 0),
            new Upgrade(UpgradeType.BulletDistance, "Upgrade shot distance", 0)
        };
            
        // TODO: reimplement with Upgrades engine
        public Upgrade[] GetHeroUpgrades()
        {
            return this.heroUpgrades;
        }

        public Upgrade GetUpgrade(UpgradeType type)
        {
            return this.heroUpgrades.First(u => u.type == type);
        }
        
        public void SetUpgrade(UpgradeType type, int level)
        {
            var upgrade = this.heroUpgrades.First(u => u.type == type);
            upgrade.currentLevel = level;
        }
    }
}