﻿using Entities;
using UnityEngine;

namespace Scripts.Services
{
    public sealed class HeroService : MonoBehaviour
    {
        [SerializeField]
        private UnityEntity hero;

        public IEntity GetHero()
        {
            return this.hero;
        }
    }
}