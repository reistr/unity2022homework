using GameElements;

namespace Scripts.Application
{
    public interface IGameDataLoader
    {
        void LoadData(IGameContext context);
    }
}