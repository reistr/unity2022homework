﻿namespace Scripts.Models
{
    public enum UpgradeType
    {
        MaxHitPoints,
        Speed,
        BulletSpeed,
        BulletDistance,
    }
    
    public class Upgrade
    {
        public UpgradeType type;

        public string label;

        public int currentLevel;
        
        public Upgrade(UpgradeType type, string label, int level)
        {
            this.type = type;
            this.label = label;
            this.currentLevel = level;
        }
    }
}