﻿using GameElements;
using Scripts.Services;
using Scripts.UI.Popups;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Test
{
    public sealed class ChangeMoneyRuntimeTest : MonoBehaviour, IGameInitElement
    {
        private MoneyStorage moneyStorage;

        [Button]
        public void AddMoney(int value)
        {
            var current = this.moneyStorage.GetMoney();
            this.moneyStorage.SetMoney(current + value);
        }
        
        [Button]
        public void SetMoney(int value)
        {
            this.moneyStorage.SetMoney(value);
        }

        void IGameInitElement.InitGame(IGameContext context)
        {
            this.moneyStorage = context.GetService<MoneyStorage>();
        }
    }
}