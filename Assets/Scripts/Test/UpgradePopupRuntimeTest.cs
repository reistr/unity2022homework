﻿using GameElements;
using Scripts.Models;
using Scripts.Services;
using Scripts.UI.Popups;
using Scripts.UI.Services;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Test
{
    public class UpgradePopupRuntimeTest : MonoBehaviour, IGameInitElement
    {
        private PopupManager popupManager;
        
        private UpgradePresentationModelFactory factory;
        
        private UpgradesStorage upgradesStorage;

        [Button]
        public void ShowPopup()
        {
            var presenter = this.factory.CreatePresenter(
               this.upgradesStorage.GetHeroUpgrades());
            this.popupManager.ShowPopup(PopupName.UPGRADE, presenter);
        }

        void IGameInitElement.InitGame(IGameContext context)
        {
            this.popupManager = context.GetService<PopupManager>();
            this.factory = context.GetService<UpgradePresentationModelFactory>();
            this.upgradesStorage = context.GetService<UpgradesStorage>();
        }
    }
}