using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Scripts.Primitives.EventReceivers
{
    public sealed class EventReceiver : MonoBehaviour
    {
        public event Action OnEvent;

        [Button]
        public void Call()
        {
            this.OnEvent?.Invoke();
        }
    }
}