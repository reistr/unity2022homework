﻿using GameElements;
using Scripts.Components;
using Scripts.Services;
using UnityEngine;

namespace Scripts.Interactors
{
    public class HeroShootInteractor : MonoBehaviour,
        IGameInitElement
    {
        private IShootComponent shootComponent;

        private IGetPositionComponent position;

        private IShootSpeedComponent shootSpeed;
        
        private IShootDistanceComponent shootDistance;
        
        private BulletShooter bulletShooter;

        public void ShootAt(Vector3 targetPosition)
        {
            this.bulletShooter.Shoot(
                this.position.GetPosition(), targetPosition, shootSpeed.GetSpeed(), shootDistance.GetMaxDistance());
            this.shootComponent.Shoot(targetPosition);
        }
        
        void IGameInitElement.InitGame(IGameContext context)
        {
            this.bulletShooter = context.GetService<BulletShooter>();
            var hero = context
                .GetService<HeroService>()
                .GetHero();
            this.shootComponent = hero.Get<IShootComponent>();
            this.shootSpeed = hero.Get<IShootSpeedComponent>();
            this.shootDistance = hero.Get<IShootDistanceComponent>();
            this.position = hero.Get<IGetPositionComponent>();
        }
    }
}