﻿using Scripts.Primitives.Beahviours;
using Scripts.Primitives.EventReceivers;
using UnityEngine;

namespace Scripts.Mechanics
{
    public sealed class MoveMechanics : MonoBehaviour
    {
        [SerializeField]
        private Vector3EventReciever moveReceiver;

        [SerializeField]
        private Transform objectTransform;

        [SerializeField]
        private FloatBehaviour speed;

        private void OnEnable()
        {
            this.moveReceiver.OnEvent += this.OnMove;
        }

        private void OnDisable()
        {
            this.moveReceiver.OnEvent -= this.OnMove;
        }

        private void OnMove(Vector3 velocity)
        {
            this.objectTransform.position += velocity * speed.Value;
        }
    }
}