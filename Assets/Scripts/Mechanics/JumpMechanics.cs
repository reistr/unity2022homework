﻿using System.Collections;
using System.Collections.Generic;
using Scripts.Primitives.Beahviours;
using Scripts.Primitives.EventReceivers;
using UnityEngine;

namespace Scripts.Mechanics
{
    public sealed class JumpMechanics : MonoBehaviour
    {
        [SerializeField]
        private EventReceiver eventReceiver;
        
        [SerializeField]
        private Transform objectTransform;

        [SerializeField]
        private FloatBehaviour jumpSpeed;

        [SerializeField]
        private FloatBehaviour jumpHeight;
        
        private float originalHeight;

        private bool isGoingDown;

        private Coroutine jumpCoroutine;
        
        private void OnEnable()
        {
            this.eventReceiver.OnEvent += this.OnJumpStart;
        }

        private void OnDisable()
        {
            this.eventReceiver.OnEvent -= this.OnJumpStart;
        }

        private void OnJumpStart()
        {
            if (this.jumpCoroutine == null)
            {
                this.originalHeight = this.objectTransform.position.y;
                this.jumpCoroutine = this.StartCoroutine(this.Jump());
            }
        }

        private IEnumerator Jump()
        {
            for (;;)
            {
                var newPosition = this.objectTransform.position +
                                (this.isGoingDown ? Vector3.down : Vector3.up) * (this.jumpSpeed.Value * Time.deltaTime);
                if (newPosition.y >= this.originalHeight + this.jumpHeight.Value)
                {
                    this.isGoingDown = true;
                    newPosition.y = this.originalHeight + this.jumpHeight.Value;
                }                
                else if (newPosition.y <= this.originalHeight)
                {
                    this.isGoingDown = false;
                    newPosition.y = this.originalHeight;
                    this.StopCoroutine(this.jumpCoroutine);
                    this.jumpCoroutine = null;
                }
                
                this.objectTransform.position = newPosition;
                yield return null;
            }
        }
    }
}