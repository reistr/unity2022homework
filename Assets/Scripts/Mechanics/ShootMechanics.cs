﻿using Scripts.Primitives.EventReceivers;
using Scripts.Services;
using UnityEngine;

namespace Scripts.Mechanics
{
    public sealed class ShootMechanics : MonoBehaviour
    {
        [SerializeField]
        private Vector3EventReciever shootReceiver;

        private void OnEnable()
        {
            this.shootReceiver.OnEvent += this.OnShoot;
        }

        private void OnDisable()
        {
            this.shootReceiver.OnEvent -= this.OnShoot;          
        }

        private void OnShoot(Vector3 targetPosition)
        {
            // Shoot effect
            
            // if (this.bulletShooter.InProgress)
            // {
            //     Debug.Log("Shoot in progress");
            //     return;
            // }
            //
            // this.bulletShooter.Shoot(this.objectTransform.position, targetPosition);
        }
    }
}