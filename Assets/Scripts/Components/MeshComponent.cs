﻿using UnityEngine;

namespace Scripts.Components
{
    public sealed class MeshComponent : MonoBehaviour, IGetBoundsComponent
    {
        [SerializeField] private MeshFilter meshFilter;
        
        public Bounds GetBounds()
        {
            return this.meshFilter.mesh.bounds;
        }
    }
}