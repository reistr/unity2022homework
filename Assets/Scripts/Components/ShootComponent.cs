﻿using Scripts.Primitives.EventReceivers;
using UnityEngine;

namespace Scripts.Components
{
    public sealed class ShootComponent : MonoBehaviour, IShootComponent
    {
        [SerializeField]
        private Vector3EventReciever shootReceiver;
        
        public void Shoot(Vector3 targetPosition)
        {
            this.shootReceiver.Call(targetPosition);
        }
    }
}