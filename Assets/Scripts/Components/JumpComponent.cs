﻿using Scripts.Primitives.EventReceivers;
using UnityEngine;

namespace Scripts.Components
{
    public class JumpComponent : MonoBehaviour, IJumpComponent
    {
        [SerializeField]
        private EventReceiver jumpReceiver;
        
        public void Jump()
        {
            this.jumpReceiver.Call();
        }
    }
}