﻿using Scripts.Primitives.Beahviours;
using UnityEngine;

namespace Scripts.Components
{
    public class ShootSpeedComponent : MonoBehaviour, IShootSpeedComponent
    {
        [SerializeField] 
        private FloatBehaviour bulletSpeed;

        public float GetSpeed()
        {
            return this.bulletSpeed.Value;
        }
    }
}