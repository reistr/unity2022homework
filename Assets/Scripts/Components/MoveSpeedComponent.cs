﻿using Scripts.Primitives.Beahviours;
using UnityEngine;

namespace Scripts.Components
{
    public sealed class MoveSpeedComponent : MonoBehaviour, IMoveSpeedComponent
    {
        [SerializeField]
        private FloatBehaviour speed;
        
        public float GetSpeed()
        {
            return this.speed.Value;
        }

        public void SetSpeed(float value)
        {
            this.speed.Value = value;
        }
    }
}