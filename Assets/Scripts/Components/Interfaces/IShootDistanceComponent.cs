﻿namespace Scripts.Components
{
    public interface IShootDistanceComponent
    {
        float GetMaxDistance(); 
    }
}