﻿namespace Scripts.Components
{
    public interface IShootSpeedComponent
    {
        float GetSpeed();
    }
}