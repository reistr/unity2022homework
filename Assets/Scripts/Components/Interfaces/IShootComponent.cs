﻿using UnityEngine;

namespace Scripts.Components
{
    public interface IShootComponent
    {
        public void Shoot(Vector3 targetPosition);
    }
}