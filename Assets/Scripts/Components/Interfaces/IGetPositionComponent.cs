﻿using UnityEngine;

namespace Scripts.Components
{
    public interface IGetPositionComponent
    {
        Vector3 GetPosition();
    }
}