﻿using UnityEngine;

namespace Scripts.Components
{
    public interface IMoveComponent
    {
        public void Move(Vector3 vector);
    }
}