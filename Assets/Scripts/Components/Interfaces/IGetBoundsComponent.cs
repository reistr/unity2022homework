﻿using UnityEngine;

namespace Scripts.Components
{
    public interface IGetBoundsComponent
    {
        public Bounds GetBounds();
    }
}