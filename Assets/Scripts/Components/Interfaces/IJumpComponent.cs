﻿namespace Scripts.Components
{
    public interface IJumpComponent
    {
        void Jump();
    }
}