﻿namespace Scripts.Components
{
    public interface IMoveSpeedComponent
    {
        float GetSpeed();

        void SetSpeed(float value);
    }
}