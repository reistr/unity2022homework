﻿using Scripts.Primitives.Beahviours;
using UnityEngine;

namespace Scripts.Components
{
    public class ShootDistanceComponent : MonoBehaviour, IShootDistanceComponent
    {
        [SerializeField] 
        private FloatBehaviour bulletMaxDistance;

        public float GetMaxDistance()
        {
            return this.bulletMaxDistance.Value;
        }
    }
}