﻿using Scripts.Primitives.EventReceivers;
using UnityEngine;

namespace Scripts.Components
{
    public sealed class MoveComponent : MonoBehaviour, IMoveComponent
    {
        [SerializeField]
        private Vector3EventReciever moveReceiver;
        
        public void Move(Vector3 vector)
        {
            this.moveReceiver.Call(vector);
        }
    }
}