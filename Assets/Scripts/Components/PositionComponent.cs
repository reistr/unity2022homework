﻿using UnityEngine;

namespace Scripts.Components
{
    public sealed class PositionComponent : MonoBehaviour, IGetPositionComponent
    {
        [SerializeField] private new Transform transform;
            
        public Vector3 GetPosition()
        {
            return this.transform.position;
        }
    }
}