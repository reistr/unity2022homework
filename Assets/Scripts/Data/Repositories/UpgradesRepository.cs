﻿using Data.Models;
using UnityEngine;

namespace Data.Repositories
{
    public sealed class UpgradesRepository
    {
        private const string PLAYER_PREFS_KEY = "Hero/Upgrade";

        public bool LoadUpgrades(out UpgradeDataWrapper data)
        {
            if (PlayerPrefs.HasKey(PLAYER_PREFS_KEY))
            {
                var json = PlayerPrefs.GetString(PLAYER_PREFS_KEY);
                data = JsonUtility.FromJson<UpgradeDataWrapper>(json);
                
                Debug.Log($"<color=orange>LOAD UPGRADES DATA {json}</color>");
                return true;
            }

            data = default;
            return false;
        }

        public void SaveUpgrades(UpgradeDataWrapper data)
        {
            var json = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(PLAYER_PREFS_KEY, json);

            Debug.Log($"<color=yellow>SAVE UPGRADES DATA {json}</color>");
        }
    }
}