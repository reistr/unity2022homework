﻿using Data.Models;
using UnityEngine;

namespace Data.Repositories
{
    public sealed class MoneyRepository
    {
        private const string PLAYER_PREFS_KEY = "Hero/Money";

        public bool LoadMoney(out MoneyData data)
        {
            if (PlayerPrefs.HasKey(PLAYER_PREFS_KEY))
            {
                var json = PlayerPrefs.GetString(PLAYER_PREFS_KEY);
                data = JsonUtility.FromJson<MoneyData>(json);
                
                Debug.Log($"<color=cyan>LOAD MONEY DATA {json}</color>");
                return true;
            }

            data = default;
            return false;
        }

        public void SaveMoney(MoneyData data)
        {
            var json = JsonUtility.ToJson(data);
            PlayerPrefs.SetString(PLAYER_PREFS_KEY, json);

            Debug.Log($"<color=lightblue>SAVE MONEY DATA {json}</color>");
        }        
    }
}