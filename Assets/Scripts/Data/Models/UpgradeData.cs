﻿using System;
using Scripts.Models;

namespace Data.Models
{
    [Serializable]
    public struct UpgradeData
    {
        public UpgradeType id;

        public int level;
    }
    
    [Serializable]
    public struct UpgradeDataWrapper
    {
        public UpgradeData[] upgrades;

        public UpgradeDataWrapper(UpgradeData[] upgrades)
        {
            this.upgrades = upgrades;
        }
    }
}