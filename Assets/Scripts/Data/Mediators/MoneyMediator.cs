﻿using Data.Models;
using Data.Repositories;
using GameElements;
using Scripts.Application;
using Scripts.Services;
using Services;

namespace Data.Mediators
{
    public sealed class MoneyMediator: IGameDataLoader, IGameDataSaver
    {
        [Inject] private MoneyRepository repository;
        
        void IGameDataLoader.LoadData(IGameContext context)
        {
            if (this.repository.LoadMoney(out MoneyData data))
            {
                var storage = context.GetService<MoneyStorage>();
                storage.SetMoney(data.amount);
            }
        }

        void IGameDataSaver.SaveData(IGameContext context)
        {
            var money = context.GetService<MoneyStorage>().GetMoney();
            this.repository.SaveMoney(new MoneyData { amount = money});
        }
        
    }
}