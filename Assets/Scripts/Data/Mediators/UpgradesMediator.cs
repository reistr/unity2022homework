﻿using Data.Models;
using Data.Repositories;
using GameElements;
using Scripts.Application;
using Scripts.Services;
using Services;

namespace Data.Mediators
{
    public sealed class UpgradesMediator: IGameDataLoader, IGameDataSaver
    {
        [Inject] private UpgradesRepository repository;
        
        void IGameDataLoader.LoadData(IGameContext context)
        {
            if (this.repository.LoadUpgrades(out UpgradeDataWrapper data))
            {
                var storage = context.GetService<UpgradesStorage>();
                foreach (var upgrade in data.upgrades)
                {
                    storage.SetUpgrade(upgrade.id, upgrade.level);
                }
            }
        }

        void IGameDataSaver.SaveData(IGameContext context)
        {
            var upgrades = context.GetService<UpgradesStorage>().GetHeroUpgrades();
            
            UpgradeData[] data = new UpgradeData[upgrades.Length];
            for (var i = 0; i < data.Length; i++)
            {
                data[i] = new UpgradeData
                {
                    id = upgrades[i].type, 
                    level = upgrades[i].currentLevel
                };
            }
            
            this.repository.SaveUpgrades(new UpgradeDataWrapper(data));
        }
    }
}