﻿using Elementary;
using GameElements;

namespace Scripts.Inputs
{
    public class InputStateManager : StateMachine<InputStateType>,
        IGameStartElement,
        IGameFinishElement
    {
        void IGameStartElement.StartGame(IGameContext context)
        {
            this.Enter();
        }
        
        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.Exit();
        }        
    }
}