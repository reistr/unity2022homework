﻿using Elementary;
using UnityEngine;

namespace Scripts.Inputs
{
    public sealed class InputStateKeyboard : State
    {
        [SerializeField]
        private KeyboardInput input;
        
        public override void Enter()
        {
            this.input.enabled = true;
        }

        public override void Exit()
        {
            this.input.enabled = false;
        }
        
    }
}