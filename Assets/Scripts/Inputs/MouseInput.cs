﻿using System;
using GameElements;
using UnityEngine;

namespace Scripts.Inputs
{
    public sealed class MouseInput : MonoBehaviour, IGameStartElement, IGameFinishElement
    {
        public event Action<Vector3> OnShoot;

        private void Awake()
        {
            this.enabled = false;
        }

        private void Update()   
        {
            this.HandleMouse();
        }
        
        private void HandleMouse()
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.Shoot();
            }
        }

        private void Shoot()
        {
            this.OnShoot?.Invoke(Input.mousePosition);
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.enabled = true;
        }

        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.enabled = false;
        }
    }
}