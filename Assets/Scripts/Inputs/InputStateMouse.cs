﻿using Elementary;
using UnityEngine;

namespace Scripts.Inputs
{
    public sealed class InputStateMouse: State
    {
        [SerializeField]
        private MouseInput input;
        
        public override void Enter()
        {
            this.input.enabled = true;
        }

        public override void Exit()
        {
            this.input.enabled = false;
        }
        
    }
}