﻿using System;
using GameElements;
using UnityEngine;

namespace Scripts.Inputs

{
    public sealed class KeyboardInput : MonoBehaviour, IGameStartElement, IGameFinishElement
    {
        public event Action<Vector3> OnMove;

        public event Action OnJump;
        
        private void Awake()
        {
            this.enabled = false;
        }
        
        private void Update()   
        {
            this.HandleKeyboard();
        }

        private void HandleKeyboard()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                this.Move(Vector3.forward);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                this.Move(Vector3.back);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                this.Move(Vector3.left);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                this.Move(Vector3.right);
            }
            
            if (Input.GetKey(KeyCode.Space))
            {
                this.Jump();
            }
        }

        private void Jump()
        {
            this.OnJump?.Invoke();
        }

        private void Move(Vector3 direction)
        {
            this.OnMove?.Invoke(direction);
        }

        public void StartGame(IGameContext context)
        {
            this.enabled = true;
        }

        public void FinishGame(IGameContext context)
        {
            this.enabled = false;
        }
    }
}