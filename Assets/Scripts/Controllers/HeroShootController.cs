﻿using GameElements;
using Scripts.Components;
using Scripts.Inputs;
using Scripts.Interactors;
using Scripts.Services;
using Scripts.Utils;
using UnityEngine;

namespace Scripts.Controllers
{
    public sealed class HeroShootController : MonoBehaviour,
        IGameInitElement,
        IGameStartElement,
        IGameFinishElement
    {
        [SerializeField]
        private float targetHeight;

        private CameraProjector cameraProjector;

        private MouseInput input;

        private HeroShootInteractor shootInteractor;

        private void Awake()
        {
            this.enabled = false;
        }

        private void OnShoot(Vector3 screenPoint)
        {
            if (this.cameraProjector.TryGetProjectedPoint(screenPoint, out Vector3 target))
            {
                var targetWithHeight = target + new Vector3(0, targetHeight, 0);

                this.shootInteractor.ShootAt(targetWithHeight);
            }
        }

        void IGameInitElement.InitGame(IGameContext context)
        {
            this.cameraProjector = context.GetService<CameraService>().CameraProjector;
            this.input = context.GetService<MouseInput>();
            this.shootInteractor = context.GetService<HeroShootInteractor>();
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.input.OnShoot += this.OnShoot;
        }

        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.input.OnShoot -= this.OnShoot;
        }
    }
}