﻿using GameElements;
using Scripts.Components;
using Scripts.Services;
using UnityEngine;

namespace Scripts.Controllers
{
    public sealed class CameraController: MonoBehaviour,
        IGameInitElement,
        IGameStartElement,
        IGameFinishElement    
    {
        private CameraService cameraService;
        
        private IGetPositionComponent heroPosition;

        private void Awake()
        {
            this.enabled = false;
        }
        
        private void LateUpdate()
        {
            this.cameraService.CameraTransform.position = 
                this.heroPosition.GetPosition() + this.cameraService.Offset;
        }
        
        void IGameInitElement.InitGame(IGameContext context)
        {
            this.cameraService = context.GetService<CameraService>();
            this.heroPosition = context.GetService<HeroService>().GetHero().Get<IGetPositionComponent>();
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.enabled = true;
        }

        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.enabled = false;
        }
    }
}