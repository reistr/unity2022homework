﻿using GameElements;
using Scripts.Components;
using Scripts.Inputs;
using Scripts.Services;
using UnityEngine;

namespace Scripts.Controllers
{
    public sealed class HeroJumpController : MonoBehaviour,
        IGameInitElement,
        IGameStartElement,
        IGameFinishElement
    {
        private IJumpComponent jumpComponent;

        private KeyboardInput input;

        private void Awake()
        {
            this.enabled = false;
        }

        private void OnJump()
        {
            this.jumpComponent.Jump();
        }

        void IGameInitElement.InitGame(IGameContext context)
        {
            this.input = context.GetService<KeyboardInput>();
            this.jumpComponent = context
                .GetService<HeroService>()
                .GetHero()
                .Get<IJumpComponent>();
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.input.OnJump += this.OnJump;
            this.enabled = true;
        }

        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.input.OnJump -= this.OnJump;
            this.enabled = false;
        }
    }
}