﻿using GameElements;
using Scripts.Components;
using Scripts.Inputs;
using Scripts.Services;
using UnityEngine;

namespace Scripts.Controllers
{
    public sealed class HeroMoveController : MonoBehaviour,
        IGameInitElement,
        IGameStartElement, 
        IGameFinishElement
    {
        private IMoveComponent moveComponent;

        private KeyboardInput input;

        private void Awake()
        {
            this.enabled = false;
        }

        private void OnMove(Vector3 direction)
        {
            this.moveComponent.Move(direction * Time.deltaTime);
        }
        
        void IGameInitElement.InitGame(IGameContext context)
        {
            this.moveComponent = context
                .GetService<HeroService>()
                .GetHero()
                .Get<IMoveComponent>();
            this.input = context.GetService<KeyboardInput>();
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.input.OnMove += this.OnMove;
            this.enabled = true;
        }

        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.input.OnMove -= this.OnMove;
            this.enabled = false;
        }
    }
}