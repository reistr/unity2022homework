﻿using System;
using GameElements;
using Scripts.Components;
using Scripts.Services;
using UnityEngine;

namespace Scripts.Controllers
{
    public sealed class FallController : MonoBehaviour,
        IGameInitElement,
        IGameStartElement,
        IGameFinishElement
    {
        private IGetPositionComponent heroPosition;
        private IGetBoundsComponent heroBounds;
        private GroundService groundService;
        private Action finishGame;

        private void Awake()
        {
            this.enabled = false;
        }
        
        private void LateUpdate()
        {
            var position = this.heroPosition.GetPosition();
            var bottomPosition = position + Vector3.down * this.heroBounds.GetBounds().extents.y;
            if (bottomPosition.y <= 0)
            {
                bottomPosition = new Vector3(bottomPosition.x, 0, bottomPosition.z);
                if (!this.groundService.IsOnGround(bottomPosition, this.heroBounds.GetBounds()))
                {
                    Debug.Log("Fail");
                    this.finishGame?.Invoke();
                }
            }
        }

        void IGameInitElement.InitGame(IGameContext context)
        {
            this.heroPosition = context.GetService<HeroService>().GetHero().Get<IGetPositionComponent>();
            this.heroBounds = context.GetService<HeroService>().GetHero().Get<IGetBoundsComponent>();
            this.groundService = context.GetService<GroundService>();
            this.finishGame = context.FinishGame;
        }

        void IGameStartElement.StartGame(IGameContext context)
        {
            this.enabled = true;
        }
        
        void IGameFinishElement.FinishGame(IGameContext context)
        {
            this.enabled = false;
        }
    }
}